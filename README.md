# Hacker News App

Hacker News App is a list of news that get data from hacker News API and maintains a communication in real time to update the data every hour or the time specified in docker-compose.yml the environment variable of the backend:

```bash
SOCKET_TIMER_HACKNEWS
```

and update enviroment url to client in:

```bash
REACT_APP_HOST_API
```

## Deploy App

step 1 : Check Docker is install, else you will be install [Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)

Step 2 : Check Docker-Compose is install, else you will be install [Docker Compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04#step-1-%E2%80%94-installing-docker-compose)

Step 3 : Clone this repo in your desktop. Then go to the new folder with name proyect

```bash
cd fullstack-test-to-reign/
```

Step 4 : Into the proyect folder execute next command:

```bash
docker-compose up
```

to make deploy of the app.

Step 5 : If everything was successful, you can view the web at:

```bash
localhost:80

or

http://YOUR_IP/
```

if you use a EC2 of AWS.
You can also see

```bash
localhost:3000/api  for the API documentation.
```

Step 6: The database is empty if you run this project on your personal machine or on another server. But you can populate this call to

```bash
localhost:3000/news/populatedb
```

## Production Server

This project will be implemented on my server for only 4 days from 01/17/2021:

[Web](http://3.134.118.118/)

[Document Api](http://3.134.118.118:3000/api/)

## Contributing

Only me

## Social

[linkedin](https://www.linkedin.com/in/jose-sleiter-rios-905447165/)
