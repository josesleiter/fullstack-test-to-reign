import React, { useEffect, useState } from "react";
import Home from "./components/pages/Home";

function App() {
  return (
    <div className="App">
      <Home></Home>
    </div>
  );
}

export default App;
