import React, { useState, useEffect } from "react";
import io from "socket.io-client";

import HomeTemplate from "./../templates/Home/Home";
import Spinner from "../atoms/Spinner/Spinner";

import newsElement from "./../../_interface/newsElement";
import { API } from "../../config/react-app-env";

const Home = () => {
  const [stateNews, setStateNews] = useState({
    news: [],
  });
  const [statePage, setStatePage] = useState({
    loading_text: "Loading...",
    loading: true,
    err: {},
  });

  useEffect((): any => {
    const socket: SocketIOClient.Socket = io(`${API}`, {
      transports: ["websocket"],
    });
    console.log("socket info:", socket);
    socket.on("getDataHackNewsAPI", (data: any) => {
      _getNews();
    });
    return () => socket.disconnect();
  }, []);

  useEffect(() => _getNews(), []);

  const _getNews: any = async () => {
    try {
      const response = await fetch(`${API}/news/all`);
      const data = await response.json();
      // console.log(data);
      if (!response.ok) {
        throw new Error("message___Get");
      }
      setStateNews({
        news: data,
      });
      setStatePage({
        ...statePage,
        loading: false,
      });
    } catch (err) {
      // const { message, code } = JSON.parse(err.message);
      setStatePage({
        err: err.message,
        loading: false,
        loading_text: "",
      });
    }
  };

  const _onHandleClick = (newsElement: newsElement) => {
    _deleteNews(newsElement);
  };

  const _deleteNews: any = async (newsElement: newsElement) => {
    try {
      setStatePage({
        ...statePage,
        loading: true,
        loading_text: "Element removing...",
      });
      const config = {
        method: "DELETE",
      };
      const response = await fetch(
        `${API}/news/delete/?id=${newsElement._id}`,
        config
      );
      const data = await response.json();
      if (!response.ok) {
        throw new Error("message___Put");
      }
      _getNews();
    } catch (err) {
      setStatePage({
        err: err.message,
        loading: false,
        loading_text: "",
      });
    }
  };

  if (statePage.loading && stateNews?.news?.length == 0)
    return (
      <Spinner show={statePage.loading} text={statePage.loading_text}></Spinner>
    );
  return (
    <div className="block-page-home">
      <Spinner show={statePage.loading} text={statePage.loading_text}></Spinner>
      <HomeTemplate
        onHandleClick={_onHandleClick}
        dataNews={stateNews.news}
        textT="HN Feed"
        textS="We <3 hacker news!"
      ></HomeTemplate>
    </div>
  );
};

export default Home;
