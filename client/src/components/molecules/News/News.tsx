import React from "react";

import Link from "../../atoms/Link/Link";
import Text from "./../../atoms/Text/Text";
import Button from "./../../atoms/Button/Button";
import Timer from "./../../atoms/Timer/Timer";

import "./News.css";

import newsElement from "./../../../_interface/newsElement";

const News = ({ data, onHandleClick }: any) => {
  const handleClick = () => {
    const newsElement: newsElement = {
      _id: data._id,
      parent_id: data.parent_id,
      author: data.author,
    };
    onHandleClick(newsElement);
  };

  return (
    <div className="Row" id={data.parent_id}>
      <Link url={!data.url ? data.story_url : data.url}>
        <div className="block-news-info">
          <div className="block-news-title">
            <Text
              type="title"
              text={!data.story_title ? data.title : data.story_title}
            ></Text>
          </div>
          <div className="block-news-author">
            <Text type="author" text={data.author}></Text>
          </div>
        </div>
        <div className="block-news-other">
          <div className="block-news-time">
            <Timer time={data.created_at_i}></Timer>
          </div>
          <Button handleClick={handleClick} icon="fa-trash"></Button>
        </div>
      </Link>
    </div>
  );
};

export default News;
