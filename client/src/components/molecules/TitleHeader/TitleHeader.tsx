import React from "react";

import Title from "./../../atoms/Title/Title";

interface IMyProps {
  textT?: String;
  textS?: String;
}

const TitleHeader = (props: IMyProps) => {
  return (
    <>
      <Title type="h1" text={props.textT}></Title>
      <Title type="" text={props.textS}></Title>
    </>
  );
};

export default TitleHeader;
