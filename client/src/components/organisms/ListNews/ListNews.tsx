import React from "react";

import News from "./../../molecules/News/News";

import newsElement from "./../../../_interface/newsElement";

interface IMyProps {
  dataNews: any;
  onHandleClick: Function;
}

const ListNews = (props: IMyProps) => {
  const onHandleClick = (newsElement: newsElement) => {
    props.onHandleClick(newsElement);
  };

  return (
    <div className="container block-news">
      {props.dataNews.map((news: any) => {
        return (
          <News key={news._id} onHandleClick={onHandleClick} data={news}></News>
        );
      })}
    </div>
  );
};

export default ListNews;
