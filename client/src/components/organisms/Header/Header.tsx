import React from "react";

import TitleHeader from "./../../molecules/TitleHeader/TitleHeader";

import "./Header.css";

const Header = (props: any) => {
  return (
    <div className="container header">
      <TitleHeader textT={props.textT} textS={props.textS}></TitleHeader>
    </div>
  );
};

export default Header;
