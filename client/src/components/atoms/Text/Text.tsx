import React from "react";

import "./Text.css";

interface IMyProps {
  type: String;
  text: String;
}

const Text = (props: IMyProps) => {
  if (props.type === "title") {
    return (
      <>
        <p className="Title"> {props.text} </p>
      </>
    );
  }
  if (props.type === "author") {
    return (
      <>
        <p className="Author"> - {props.text} - </p>
      </>
    );
  }
  return (
    <>
      <p className="Another">{props.text}</p>
    </>
  );
};

export default Text;
