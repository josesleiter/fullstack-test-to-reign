import React from "react";
import moment from "moment";

interface IMyProps {
  time: any;
}

const Timer = (props: IMyProps) => {
  const formats = {
    nextDay: "[Tomorrow]",
    nextWeek: "dddd",
    sameDay: "hh:mm a",
    lastDay: "[Yesterday]",
    lastWeek: "DD MMM yyyy",
    sameElse: "DD MMM yyyy",
  };

  const timestamp_to_date = Number(props.time) * 1000;

  const date_to_string = moment(new Date(timestamp_to_date)).calendar(formats);

  return (
    <>
      <p className="Time">{date_to_string}</p>
    </>
  );
};

export default Timer;
