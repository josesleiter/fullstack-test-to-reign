import React from "react";

import icon from "./../../../assets/img/iconmonstr-trash-can-1.svg";
import "./Button.css";

interface IMyProps {
  handleClick: Function;
  icon?: String;
  text?: String;
}

const Button = (props: IMyProps) => {
  const handleClick = (e: any) => {
    e.preventDefault();
    props.handleClick();
  };

  return (
    <div className="btn-content">
      <button onClick={handleClick} className="btn">
        <span className={`icon ${!props.icon ? "is-hidden" : ""}`}>
          <img src={icon} alt="" />
        </span>
        <span>{props.text}</span>
      </button>
    </div>
  );
};

export default Button;
