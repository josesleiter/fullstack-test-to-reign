import React from "react";

import "./Title.css";

interface IMyProps {
  type?: String;
  text?: String;
}
const Title = (props: IMyProps) => {
  if (props.type === "h1") {
    return (
      <>
        <h1 className="textT">{props.text}</h1>
      </>
    );
  }
  return (
    <>
      <h2 className="textS">{props.text}</h2>
    </>
  );
};

export default Title;
