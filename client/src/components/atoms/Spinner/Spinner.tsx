import React from "react";
import "./Spinner.css";

interface IMyProps {
  text?: String;
  show?: Boolean;
}

export default (props: IMyProps) => {
  return (
    <div
      id="spinner-modal-background"
      className={props.show ? "show-spinner" : "hidden-spinner"}
    >
      <div className="content-spinner">{props.text}</div>
    </div>
  );
};
