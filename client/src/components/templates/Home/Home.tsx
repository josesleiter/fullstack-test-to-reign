import React from "react";

import Header from "./../../organisms/Header/Header";
import ListNews from "./../../organisms/ListNews/ListNews";

import "./Home.css";

import newsElement from "./../../../_interface/newsElement";

interface IMyProps {
  dataNews: any;
  textT: String;
  textS?: String;
  onHandleClick: Function;
}

const Home = (props: IMyProps) => {
  const onHandleClick = (newsElement: newsElement) => {
    props.onHandleClick(newsElement);
  };

  return (
    <div className="block-t-home">
      <header className="t-header">
        <Header textT={props.textT} textS={props.textS}></Header>
      </header>
      <section className="t-content">
        <ListNews
          onHandleClick={onHandleClick}
          dataNews={props.dataNews}
        ></ListNews>
      </section>
    </div>
  );
};

export default Home;
