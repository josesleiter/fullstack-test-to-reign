import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class NewsDto {
  @ApiProperty()
  @IsString()
  text: string;

  @ApiProperty()
  @IsString()
  title: String;

  @ApiProperty()
  @IsString()
  url: String;

  @ApiProperty()
  @IsString()
  author: String;

  @ApiProperty()
  @IsNumber()
  story_id: Number;

  @ApiProperty()
  @IsString()
  story_title: String;

  @ApiProperty()
  @IsString()
  story_url: String;

  @ApiProperty()
  @IsNumber()
  parent_id: Number;

  @ApiProperty()
  @IsBoolean()
  is_deleted: Boolean;

  @ApiProperty()
  created_at_i: Date;
}
