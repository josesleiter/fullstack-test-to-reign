import { Logger } from '@nestjs/common';
import {
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { NewsService } from './../service/news.service';

@WebSocketGateway({
  transports: ['websocket'],
})
export class SocketGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;
  private logger: Logger = new Logger('socketGateway');

  constructor(private NewsService: NewsService) {}

  afterInit(server: Server) {
    this.logger.log('Initialized...');
    setInterval(async () => {
      await this.NewsService.findApi();
      this.server.emit('getDataHackNewsAPI', {});
    }, Number(process.env.SOCKET_TIMER_HACKNEWS));
  }

  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  // @SubscribeMessage('events1')
  // handleEvent1(@MessageBody() data: unknown): WsResponse<unknown> {
  //   const event = 'events1';
  //   console.log('mierda');
  //   return { event, data };
  // }

  // @SubscribeMessage('events2')
  // handleEvent2(@MessageBody() data: unknown): WsResponse<unknown> {
  //   const event = 'events2';
  //   console.log('mierda');
  //   return { event, data };
  // }

  // @SubscribeMessage('events')
  // async identity(@MessageBody() data: number): Promise<number> {
  //   console.log(data);
  //   return data;
  // }

  // @SubscribeMessage('events')
  // handleEvent(@MessageBody() data: string): string {
  //   return data;
  // }
}
