import * as mongoose from 'mongoose';

const NewsSchema = new mongoose.Schema({
  title: String,
  url: String,
  author: String,
  story_id: Number,
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: String,
  is_deleted: { type: Boolean, default: false },
});

export default NewsSchema;
