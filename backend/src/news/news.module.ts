import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { NewsController } from './controller/news.controller';
import { NewsService } from './service/news.service';
import { SocketGateway } from './gateway/socket.gateway';
import NewsSchema from './schema/news.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'News', schema: NewsSchema }]),
    HttpModule,
  ],
  controllers: [NewsController],
  providers: [NewsService, SocketGateway],
})
export class NewsModule {}
