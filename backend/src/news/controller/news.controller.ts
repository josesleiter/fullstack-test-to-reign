import {
  Controller,
  Res,
  Query,
  Get,
  HttpStatus,
  Post,
  Body,
  Param,
  NotFoundException,
  Put,
  Delete,
} from '@nestjs/common';

import { NewsService } from './../service/news.service';
import { NewsDto } from './../dto/news.dto';

@Controller('news')
export class NewsController {
  constructor(private NewsService: NewsService) {}

  @Post('/create')
  async addNews(@Res() res, @Body() CreateNews: NewsDto) {
    const lists = await this.NewsService.create(CreateNews);
    return res.status(HttpStatus.OK).json({
      message: 'Post has been created successfully',
      lists,
    });
  }

  @Get('/all')
  async findAll(@Res() res) {
    const lists = await this.NewsService.findAllSortCreatedAt();
    return res.status(HttpStatus.OK).json(lists);
  }

  @Get('/populatedb')
  async findApi(@Res() res) {
    const lists = await this.NewsService.findApi();
    return res.status(HttpStatus.OK).json(lists);
  }

  @Delete('/delete')
  async delete(@Res() res, @Query('id') id: string) {
    const lists = await this.NewsService.delete(id);
    if (!lists) throw new NotFoundException('Post does not exist');
    return res.status(HttpStatus.OK).json({
      message: 'Post has been deleted',
      lists,
    });
  }

  //   @Get('id')
  //   async findById(@Res() res, @Query('id') id: string) {
  //     const lists = await this.NewsService.findById(id);
  //     if (!lists) throw new NotFoundException('Id does not exist!');
  //     return res.status(HttpStatus.OK).json(lists);
  //   }

  //   @Put('/update')
  //   async update(
  //     @Res() res,
  //     @Query('id') id: string,
  //     @Body() CreateAboutDTO: NewsDto,
  //   ) {
  //     const lists = await this.NewsService.update(id, CreateAboutDTO);
  //     if (!lists) throw new NotFoundException('Id does not exist!');
  //     return res.status(HttpStatus.OK).json({
  //       message: 'Post has been successfully updated',
  //       lists,
  //     });
  //   }
}
