import { Document } from 'mongoose';

export interface INews extends Document {
  readonly text: string;
  readonly title: String;
  readonly url: String;
  readonly author: String;
  readonly story_id: Number;
  readonly story_title: String;
  readonly story_url: String;
  readonly parent_id: Number;
  readonly created_at_i: Date;
  is_deleted: Boolean;
}
