import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { INews } from './../interfaces/news.interface';
import { NewsDto } from './../dto/news.dto';

@Injectable()
export class NewsService {
  constructor(
    @InjectModel('News')
    private NewsModel: Model<INews>,
    private httpService: HttpService,
  ) {}

  /**
   * Get request to 'api hacker news' to get
   * the data once a hour
   */
  async findApi(): Promise<any> {
    const {
      data: { hits: _newsListApi },
    } = await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();

    if (_newsListApi.length > 1) {
      Promise.all(
        _newsListApi.map(async (news) => {
          return await this.create(news);
        }),
      );
    }
    return await this.NewsModel.find();
  }

  /**
   * Find All news order by create_at_i
   */
  async findAllSortCreatedAt(): Promise<any> {
    return await this.NewsModel.find({ is_deleted: false })
      .sort({ created_at_i: 'desc' })
      .exec();
  }

  /**
   * Create a new news, but if the news is exist in
   * DB, check that it was not remove or his attribute
   * 'is_deleted' will update to true. Other case
   * 'is_deleted' is false
   *
   * @param news
   */
  async create(news: NewsDto): Promise<any> {
    const { author, story_id } = news;
    const _news = await this.NewsModel.findOne({
      author,
      story_id,
      is_deleted: true,
    }).exec();

    if (_news) {
      news.is_deleted = true;
    }

    const createdNews = new this.NewsModel(news);
    return createdNews.save();
  }

  /**
   * Delete a news by id, if exist others news with
   * same author and parent_id they will be removed
   * @param id
   */
  async delete(id): Promise<any> {
    const _news = await this.NewsModel.findById(id).exec();
    _news.is_deleted = true;

    if (_news) {
      const { author, story_id } = _news;
      const listNews = await this.NewsModel.find({ author, story_id });
      if (listNews.length > 1) {
        Promise.all(
          listNews.map(async (news) => {
            news.is_deleted = true;
            return await this.NewsModel.findByIdAndUpdate(news._id, news, {
              new: true,
            });
          }),
        );
        return await this.NewsModel.find({ author, story_id });
      }
    }

    return await this.NewsModel.findByIdAndUpdate(id, _news, {
      new: true,
    });
  }

  // async findOne(req): Promise<any> {
  //   return await this.NewsModel.findOne(req).exec();
  // }

  // async findById(id): Promise<INews> {
  //   const news = await this.NewsModel.findById(id).exec();
  //   return news;
  // }

  // async find(req): Promise<any> {
  //   return await this.NewsModel.find(req).exec();
  // }

  // async update(id, news: NewsDto): Promise<any> {
  //   return await this.NewsModel.findByIdAndUpdate(id, news, {
  //     new: true,
  //   });
  // }
}
